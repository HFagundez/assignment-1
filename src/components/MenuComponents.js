import React, { Component } from 'react';

import DishDetail from './DishdetailComponent';
import { Card, CardImg, CardImgOverlay,  CardTitle } from 'reactstrap';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedDish: null               
        };
        console.log('Menu Component constructor is invoked');
    }

    componentDidMount() {
      console.log('Menu Component componentDidMount is invoked');
    }

    onDishSelect(dish){
      this.setState({selectedDish: dish});
    }

    render() {
        const menu = this.props.dishes.map((dish) => {
            return (
              <div key={dish.id} className="col-12 col-sm-5 m-1">
                <Card onClick={() => this.onDishSelect(dish)}>
                  <CardImg width="100%" src={dish.image} alt={dish.name} />
                  <CardImgOverlay>
                    <CardTitle className="text-uppercase text-left">{dish.name}</CardTitle>
                  </CardImgOverlay>
                </Card>
              </div>
            );
        });

        console.log('Menu Component render is invoked');

        return (
          <div className="container" style={{marginTop: 4+'rem' , marginBottom: 1+'rem'}}>
            <div className="row"> 
                  {menu}
            </div>
            <div className="container " style={{ marginTop: 2 + 'rem', marginBottom: 1 + 'rem' }}>
              <div className="row">
                <DishDetail selectedDish={this.state.selectedDish} />
              </div>
            </div>
          </div>
        );
    }
}

export default Menu;