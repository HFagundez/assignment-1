import React from 'react';
import { Card, CardImg,  CardText, CardBody, CardTitle } from 'reactstrap';


class DishDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  renderDish(dish) {
      if (dish != null) {
        return(
          <Card>
            <CardImg width="100%" src={dish.image} alt={dish.name} />
            <CardBody>
              <CardTitle className="text-uppercase text-left">{dish.name}</CardTitle>
              <CardText className="font-italic text-center font-weight-bold">{dish.description}</CardText>
            </CardBody> 
          </Card> 
        )
      }
      else {
        return(
          <div></div>
        )
      }
    } 

    renderComments(dish){
        
        if (dish != null) {
          

           const formatDate = (datestring) => { 
            const Months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
            let d = new Date(datestring);
            return `${Months[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()} - ${d.getHours()}:${d.getUTCMinutes()}`
            } 
          
          const comments = dish.comments.map((com) =>   
           
            <li key={com.id} className=" mb-2">
              <h5 className="text-center mb-0 text-warning font-weight-bold">{com.comment}</h5>
              <p className="text-right mb-0  font-italic ">{com.author}</p>
              <p className="text-right mb-0 font-weight-light">{formatDate(com.date)}</p>
            </li>
           
          );

        return(
        
          <div className="text-white">
              <h2 className="">Comments</h2>
              <ul className="list-unstyled">
                {comments}
              </ul>
            </div>
        )
      }
      else {
        return(
          <div></div>
        )
      }
      }
    
    
      render() {
        return (
          <div className="row">
            <div className="col-12 col-md-5 m-1">
              {this.renderDish(this.props.selectedDish)}
            </div>
            <div className="col-12 col-md-5 m-1">
              {this.renderComments(this.props.selectedDish)}
              
            </div>
          </div>
        );
      }
    }
    
    export default DishDetail;